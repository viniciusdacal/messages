## Technical decisions

Most dependencies were updated to use latest version.
Pre-commit was configured to lint and format the code.
A new package.json script was included: `test:coverage` and the coverage configuration was fixed.

## Patterns

**Naming components**
Components are named using what I call NAIL (Name As Its Location).
If a component is located at `pages/dashboard/index.tsx`, it should be named `PagesDashboard` for example.

For components located inside the `components` folder, we just ignore the prefix `components` to avoid redundancy.

Examples:

```
components/Message/Card/Card.tsx -> MessageCard
components/Message/Dashboard/Dashboard.tsx -> MessageDashboard
components/UI/Notification/Notification.tsx -> UINotification
```

**Why?**
Basically, naming this way helps when you need to find the file inside the project.
If you inspect the app through the browser with the `react-dev-tools`, it will show `MessageCard` in the component tree, so
it's easy to gess that this file is located at `components/Message/Card`.
Also, if you just search for `MessageCard` in VS Code or Sublime Text, it usually brings the expected file already.

**Avoid overuse of index files**
I like to avoid the use of index files inside the components folder, otherwise, when coding you usually end up with a lot of text editor tabs with the name `index.tsx`, and that's not helpfull at all.

**Group code into domains**
I like to group the components and related files into application domains (or packages if you like).
For this application, we have a single domain, which is Message.
Everything related to message, i put into that folder. That usually reduces cognitive effort because it keeps things closer.
If a developer is working on a feature, it doesn't need to keep jumping through the whole project.

**Pages**
Pages folder would have a root file which could hold the root configuration for the routes if using react router.
For each route of the application, we can create a folder with the same name. In this case, as we don't have routes, I just created /dashboard to exemplify. And usually, in this folder I try to keep it very tight, without the business logic. It main purpose is to put the components page together and hold page level configs if needed. Page title for example.
Also, this is very similar to what nextjs does, so, it helps to keep the same pattern being a nextjs project or not.

**Use contextual names for files**
When naming the files I like to give just a contextual name and avoiding redundancies.
For example, instead of having `components/Message/MessageCard`, I just have `components/Messsage/Card` because we know this card is related to message through its location.
