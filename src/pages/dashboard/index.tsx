import MessageDashboard from 'components/Message/Dashboard/Dashboard';
import Typography from '@mui/material/Typography';
import MessageContextProvider from 'components/Message/Context/Provider';

export default function PagesDashboard() {
  return (
    <MessageContextProvider>
      <header>
        <Typography variant="h4" component="h1" data-testid="page_header">
          nuffsaid.com Coding Challenge
        </Typography>
      </header>
      <hr />
      <MessageDashboard />
    </MessageContextProvider>
  );
}
