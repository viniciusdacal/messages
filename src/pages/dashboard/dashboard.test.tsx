import { render, screen, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom';
import PagesDashboard from './index';

jest.mock('../../components/Message/feed');

let columnheaders: HTMLElement[] = [];

beforeAll(() => {
  render(<PagesDashboard />);
  columnheaders = screen.getAllByRole('columnheader');
});

test('should display page title', () => {
  expect(screen.getByTestId('page_header')).toHaveTextContent(
    'nuffsaid.com Coding Challenge'
  );
});

test('should display column titles', () => {
  expect(columnheaders[0].querySelector('h3')).toHaveTextContent(
    'Error type 1'
  );
  expect(columnheaders[1].querySelector('h3')).toHaveTextContent(
    'Warning type 2'
  );
  expect(columnheaders[2].querySelector('h3')).toHaveTextContent('Info type 3');
});

test('should display counts', () => {
  expect(columnheaders[0].querySelector('span')).toHaveTextContent('Count 0');
  expect(columnheaders[1].querySelector('span')).toHaveTextContent('Count 0');
  expect(columnheaders[2].querySelector('span')).toHaveTextContent('Count 0');
});

test('should have toggle stop/start button text when clicked', async () => {
  const { getByText } = render(<PagesDashboard />);

  fireEvent.click(getByText('STOP'));

  expect(getByText('START')).toBeInTheDocument();
});
