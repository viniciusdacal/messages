import { render, fireEvent, act, within } from '@testing-library/react';
import '@testing-library/jest-dom';
import { Message, MessagePriority } from 'components/Message/types';
import PagesDashboard from './index';

jest.mock('../../components/Message/feed');
const controller = jest.requireMock('../../components/Message/feed').controller;

const fireMessages = (messages: Message[]) => {
  act(() => {
    messages.forEach((message) => {
      controller.generate(message);
    });
  });
};

test('should display a notification when receiving an error message', () => {
  const { getByTestId } = render(<PagesDashboard />);
  fireMessages([
    {
      message: 'this is an error message',
      priority: MessagePriority.Error,
    },
  ]);

  expect(getByTestId('UINotification')).toHaveTextContent(
    'this is an error message'
  );
});

test('should display messages in the respective columns', async () => {
  const { getByTestId } = render(<PagesDashboard />);

  fireMessages([
    {
      message: 'this is an error message',
      priority: MessagePriority.Error,
    },
    {
      message: 'this is a warning message',
      priority: MessagePriority.Warn,
    },
    {
      message: 'this is an info message',
      priority: MessagePriority.Info,
    },
  ]);

  const errorColumn = getByTestId(
    `MessageDashboard__column_${MessagePriority.Error}`
  );
  const warnColumn = getByTestId(
    `MessageDashboard__column_${MessagePriority.Warn}`
  );
  const infoColumn = getByTestId(
    `MessageDashboard__column_${MessagePriority.Info}`
  );

  expect(errorColumn).toHaveTextContent('this is an error message');
  expect(warnColumn).toHaveTextContent('this is a warning message');
  expect(infoColumn).toHaveTextContent('this is an info message');
});

test('should add new messages to the top of the list', async () => {
  const { getAllByTestId } = render(<PagesDashboard />);

  fireMessages([
    {
      message: 'this is an error message',
      priority: MessagePriority.Error,
    },
  ]);

  expect(getAllByTestId('MessageCard')[0]).toHaveTextContent(
    'this is an error message'
  );

  fireMessages([
    {
      message: 'this is a second error message',
      priority: MessagePriority.Error,
    },
  ]);

  expect(getAllByTestId('MessageCard')[0]).toHaveTextContent(
    'this is a second error message'
  );
});

test('should be able to clear a message', async () => {
  const { getByTestId } = render(<PagesDashboard />);

  fireMessages([
    {
      message: 'this is an error message',
      priority: MessagePriority.Error,
    },
  ]);

  const errorColumn = getByTestId(
    `MessageDashboard__column_${MessagePriority.Error}`
  );

  expect(errorColumn).toHaveTextContent('this is an error message');

  const clearButton = within(getByTestId('MessageCard')).getByText('Clear');
  fireEvent.click(clearButton);

  expect(errorColumn).not.toHaveTextContent('this is an error message');
});

test('should be able to clear all messages', async () => {
  const { getByTestId } = render(<PagesDashboard />);

  fireMessages([
    {
      message: 'this is an error message',
      priority: MessagePriority.Error,
    },
    {
      message: 'this is a second error message',
      priority: MessagePriority.Error,
    },
    {
      message: 'this is an info message',
      priority: MessagePriority.Info,
    },
    {
      message: 'this is a warning message',
      priority: MessagePriority.Warn,
    },
  ]);

  const errorColumn = getByTestId(
    `MessageDashboard__column_${MessagePriority.Error}`
  );
  const warnColumn = getByTestId(
    `MessageDashboard__column_${MessagePriority.Warn}`
  );
  const infoColumn = getByTestId(
    `MessageDashboard__column_${MessagePriority.Info}`
  );

  expect(errorColumn).toHaveTextContent('this is an error message');
  expect(errorColumn).toHaveTextContent('this is a second error message');
  expect(warnColumn).toHaveTextContent('this is a warning message');
  expect(infoColumn).toHaveTextContent('this is an info message');

  const clearAllButton = within(
    getByTestId('MessageDashboard__actions')
  ).getByText('CLEAR');
  fireEvent.click(clearAllButton);

  expect(errorColumn).not.toHaveTextContent('this is an error message');
  expect(errorColumn).not.toHaveTextContent('this is a second error message');
  expect(warnColumn).not.toHaveTextContent('this is a warning message');
  expect(infoColumn).not.toHaveTextContent('this is an info message');
});

test('should be able to toggle messages stream', async () => {
  const { getByTestId, getByText } = render(<PagesDashboard />);

  fireMessages([
    {
      message: 'this is an error message',
      priority: MessagePriority.Error,
    },
  ]);

  const errorColumn = getByTestId(
    `MessageDashboard__column_${MessagePriority.Error}`
  );

  expect(errorColumn).toHaveTextContent('this is an error message');

  const stopButton = getByText('STOP');
  fireEvent.click(stopButton);

  fireMessages([
    {
      message: 'this is a second error message',
      priority: MessagePriority.Error,
    },
  ]);

  expect(errorColumn).not.toHaveTextContent('this is a second error message');

  fireEvent.click(stopButton);

  fireMessages([
    {
      message: 'this is a third error message',
      priority: MessagePriority.Error,
    },
  ]);

  expect(errorColumn).toHaveTextContent('this is a third error message');
});
