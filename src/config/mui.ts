import { createTheme } from '@mui/material';

export const theme = createTheme({
  components: {
    MuiButton: {
      variants: [
        {
          props: { color: 'success' },
          style: {
            ':hover': {
              backgroundColor: '#88FCA3',
              color: '#152016',
            },
            backgroundColor: '#88FCA3',
            color: '#152016',
            fontWeight: 'bold',
          },
        },
      ],
    },
  },
  typography: {
    fontFamily: '"Times New Roman", Times, serif',
    button: {
      fontFamily: [
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
      ].join(','),
    },
    h5: {
      fontWeight: 'bold',
    },
  },
});
