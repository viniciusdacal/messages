import { useContext } from 'react';
import MessageContext from './Context';

export default function useMessages() {
  return useContext(MessageContext);
}
