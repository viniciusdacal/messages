import { createContext } from 'react';
import { MessagePriority, Message } from 'components/Message/types';

export const initialMessages: { [key in MessagePriority]: Message[] } = {
  [MessagePriority.Error]: [],
  [MessagePriority.Warn]: [],
  [MessagePriority.Info]: [],
};

const MessageContext = createContext({
  isListening: true,
  messages: initialMessages,
  onClearAllMessages: () => {},
  onClearMessage: (message: Message) => {},
  toggleListener: () => {},
});

export default MessageContext;
