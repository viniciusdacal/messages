import { useCallback, useState, useEffect } from 'react';
import subscribe from 'components/Message/feed';
import { Message, MessagePriority } from 'components/Message/types';
import MessageContext, { initialMessages } from './Context';
import UINotification from 'components/UI/Notification/Notification';

type MessageContextProviderProps = WithChildren<{}>;

const MessageContextProvider = ({ children }: MessageContextProviderProps) => {
  const [isSnackbarOpen, setIsSnackbarOpen] = useState(false);
  const [messages, setMessages] = useState(initialMessages);
  const [isListening, setIsListening] = useState(true);

  const errorMessages = messages[MessagePriority.Error];

  const onClearMessage = useCallback((message: Message) => {
    setMessages((oldMessages) => {
      if (message.priority === MessagePriority.Error) {
        const messageIndex = oldMessages[message.priority].indexOf(message);

        if (messageIndex === 0) {
          setIsSnackbarOpen(false);
        }
      }

      return {
        ...oldMessages,
        [message.priority]: oldMessages[message.priority].filter(
          (stateMessage) => stateMessage !== message
        ),
      };
    });
  }, []);

  const onClearAllMessages = useCallback(() => {
    setMessages(initialMessages);
  }, []);

  const toggleListener = useCallback(() => {
    setIsListening((oldIsListening) => !oldIsListening);
  }, []);

  useEffect(() => {
    if (isListening) {
      const unsubscribe = subscribe((message: Message) => {
        if (message.priority === MessagePriority.Error) {
          setIsSnackbarOpen(true);
        }
        setMessages((oldMessages) => ({
          ...oldMessages,
          [message.priority]: [message, ...oldMessages[message.priority]],
        }));
      });
      return unsubscribe;
    }
  }, [isListening]);

  return (
    <MessageContext.Provider
      value={{
        isListening,
        messages,
        onClearAllMessages,
        onClearMessage,
        toggleListener,
      }}
    >
      <UINotification
        onClose={() => setIsSnackbarOpen(false)}
        open={isSnackbarOpen && Boolean(errorMessages[0]?.message)}
        message={errorMessages[0]?.message}
        severity="error"
      />

      {children}
    </MessageContext.Provider>
  );
};

export default MessageContextProvider;
