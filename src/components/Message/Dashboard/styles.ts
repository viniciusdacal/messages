import styled from 'styled-components';
import Grid from '@mui/material/Grid';

export const Actions = styled(Grid)`
  margin-bottom: 3.125rem;
`;
