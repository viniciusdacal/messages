import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Stack from '@mui/material/Stack';
import Typography from '@mui/material/Typography';
import useMessages from 'components/Message/Context/useMessages';
import MessageCard from 'components/Message/Card/Card';
import { MessagePriority } from 'components/Message/types';

import * as S from './styles';

const priorities = [
  { label: 'Error', key: MessagePriority.Error },
  { label: 'Warning', key: MessagePriority.Warn },
  { label: 'Info', key: MessagePriority.Info },
];

const MessageDashboard = () => {
  const {
    messages,
    toggleListener,
    isListening,
    onClearAllMessages,
    onClearMessage,
  } = useMessages();

  return (
    <Container maxWidth="xl">
      <S.Actions
        container
        direction="row"
        justifyContent="center"
        data-testid={`MessageDashboard__actions`}
      >
        <Stack spacing={1} direction="row">
          <Button variant="contained" color="success" onClick={toggleListener}>
            {isListening ? 'STOP' : 'START'}
          </Button>
          <Button
            variant="contained"
            color="success"
            onClick={onClearAllMessages}
          >
            CLEAR
          </Button>
        </Stack>
      </S.Actions>
      <Grid container spacing={2}>
        {priorities.map(({ key, label }) => (
          <Grid
            item
            xs={3}
            md={4}
            key={key}
            data-testid={`MessageDashboard__column_${key}`}
          >
            <header role="columnheader">
              <Typography variant="h5" component="h3">
                {label} type {key + 1}
              </Typography>
              <Typography variant="subtitle1" component="span">
                Count {messages[key].length}
              </Typography>
            </header>
            <Stack spacing={1}>
              {messages[key]?.map?.((msg) => (
                <MessageCard
                  key={msg.message}
                  priority={msg.priority}
                  onClear={() => onClearMessage(msg)}
                >
                  {msg.message}
                </MessageCard>
              ))}
            </Stack>
          </Grid>
        ))}
      </Grid>
    </Container>
  );
};

export default MessageDashboard;
