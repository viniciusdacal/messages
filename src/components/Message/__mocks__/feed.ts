import { Observable } from 'rxjs';
import { Message } from 'components/Message/types';

export const controller = {
  generate: (message: Message) => {},
};

const observable = new Observable<Message>((subscriber) => {
  controller.generate = (message: Message) => {
    subscriber.next(message);
  };
});

const subscribe = (callback: (message: Message) => void) => {
  const subscription = observable.subscribe({
    next: callback,
  });
  return () => subscription.unsubscribe();
};

export default subscribe;
