import React from 'react';
import CardContent from '@mui/material/CardContent';
import { MessagePriority } from 'components/Message/types';

import * as S from './styles';

type MessageCardProps = WithChildren<{
  priority: MessagePriority;
  onClear: React.MouseEventHandler;
}>;

const MessageCard = ({ priority, children, onClear }: MessageCardProps) => (
  <S.Container data-testid="MessageCard" priority={priority}>
    <CardContent>{children}</CardContent>
    <S.Actions>
      <S.ClearButton onClick={onClear} size="small">
        Clear
      </S.ClearButton>
    </S.Actions>
  </S.Container>
);

export default MessageCard;
