import Card from '@mui/material/Card';
import styled from 'styled-components';
import { MessagePriority } from 'components/Message/types';
import CardActions from '@mui/material/CardActions';
import Button from '@mui/material/Button';

const containerBgColors = {
  [MessagePriority.Error]: '#F56236',
  [MessagePriority.Warn]: '#FCE788',
  [MessagePriority.Info]: '#88FCA3',
};

export const Container = styled(Card)<{ priority: MessagePriority }>`
  background-color: ${({ priority }) => containerBgColors[priority]};
`;

export const Actions = styled(CardActions)`
  justify-content: flex-end;
`;

export const ClearButton = styled(Button)`
  color: #152016;
  text-transform: none;
  font-size: 0.9375rem;
`;
