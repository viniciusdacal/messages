import { render } from '@testing-library/react';
import '@testing-library/jest-dom';
import { MessagePriority } from 'components/Message/types';
import MessageCard from './Card';

describe('components/Message/Card', () => {
  it('should display proper colors accordingly to priorities', () => {
    const { getByTestId, rerender } = render(
      <MessageCard priority={MessagePriority.Error} onClear={() => {}}>
        this is an error message
      </MessageCard>
    );

    expect(getByTestId('MessageCard')).toHaveStyle('background-color: #F56236');

    rerender(
      <MessageCard priority={MessagePriority.Warn} onClear={() => {}}>
        this is a warning message
      </MessageCard>
    );

    expect(getByTestId('MessageCard')).toHaveStyle('background-color: #FCE788');

    rerender(
      <MessageCard priority={MessagePriority.Info} onClear={() => {}}>
        this is an info message
      </MessageCard>
    );

    expect(getByTestId('MessageCard')).toHaveStyle('background-color: #88FCA3');
  });
});
