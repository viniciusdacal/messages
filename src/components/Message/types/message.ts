import { MessagePriority } from './priority';

export type Message = {
  message: string;
  priority: MessagePriority;
};
