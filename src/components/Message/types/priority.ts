export enum MessagePriority {
  Error,
  Warn,
  Info,
}
