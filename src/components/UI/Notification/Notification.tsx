import Snackbar, { SnackbarProps } from '@mui/material/Snackbar';
import MuiAlert, { AlertColor } from '@mui/material/Alert';

type UINotificationProps = SnackbarProps & {
  message: string;
  severity: AlertColor;
};

const UINotification = ({
  open,
  onClose,
  message,
  severity,
}: UINotificationProps) => (
  <Snackbar
    data-testid="UINotification"
    onClose={(event, reason) => {
      if (onClose && reason !== 'clickaway') {
        onClose(event, reason);
      }
    }}
    autoHideDuration={2000}
    open={open && Boolean(message)}
    key={message}
    anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
  >
    <MuiAlert severity={severity} variant="filled">
      {message}
    </MuiAlert>
  </Snackbar>
);

export default UINotification;
