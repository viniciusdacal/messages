import React from 'react';
import ReactDOM from 'react-dom';
import { ThemeProvider } from '@mui/material';
import PagesRoot from './pages/root';
import reportWebVitals from './reportWebVitals';

import { theme } from 'config/mui';

ReactDOM.render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <PagesRoot />
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
